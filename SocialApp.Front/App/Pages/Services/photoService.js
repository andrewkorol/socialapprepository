﻿angular.module('photos')
    .factory('photosService', [
        '$http',
        function ($http) {
            return {
                getPhotos: getPhotos
            };
            function getPhotos() {
                return $http.get('https://localhost:44363/api/photos/bdffdcda-1b02-44f4-86a4-1c82037772c7')
                    .then($http.getDataFromResult);
            }
        }]);

//    $http.get('https://localhost:44363/api/photos/bdffdcda-1b02-44f4-86a4-1c82037772c7')
//        .then(function (response) {
//            $scope.photosData = response.data;
//            console.log(response);
//        }, function (error) {
//            console.log(error);
//        });
//})