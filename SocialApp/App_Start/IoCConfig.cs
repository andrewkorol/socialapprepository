﻿using Microsoft.Practices.Unity;
using SocialAppBuisnessLogic;
using SocialAppDataAccess;
using SocialAppDataAccess.Contracts;
using SocialAppDomain.Interfaces;
using System.Web.Http;

namespace SocialApp.App_Start
{
    public class IoCConfig
    {
        public static void RegisterDependencies(HttpConfiguration config)
        {
            var container = new UnityContainer();
            container.RegisterType<IUserPhotoRepository, UserPhotoRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ICalculateRatingService, CalculateRatingService>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);
        }

    }
}