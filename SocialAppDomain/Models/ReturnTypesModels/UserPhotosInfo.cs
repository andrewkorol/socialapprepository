﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialAppDomain.Models.ReturnTypesModels
{
    public class UserPhotosInfo
    {
        public string userName { get; set; }
        public IEnumerable<PhotoInfo> info { get; set; }
    }
}
