﻿angular.module('photos', [])
    .config([
        '$locationProvider',
        '$provide',
        function($locationProvider, $provide){
            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });

            $provide.decorator('$http', function($delegate){
                $delegate.getDataFromResult = function(response) {
                    return response.data;
                };
                return $delegate;
            });
        }]);