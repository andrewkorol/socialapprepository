﻿using SocialAppDomain.Models;
using SocialAppDomain.Models.ReturnTypesModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SocialApp.Controllers
{
    public class UsersController : ApiController
    {
        ApplicationDbContext context = new ApplicationDbContext();
        public IEnumerable<UsersInfo> Get()
        {
            return context.Users.Select(u => new UsersInfo
            {
                UserName = u.UserName,
                Email = u.Email,
                Hometown = u.Hometown
            });
        }
    }
}
