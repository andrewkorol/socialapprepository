﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialAppDomain.Models.PhotoModels
{
    public class PhotoCalculatedRating
    {
        public int Id { get; set; }
        public int newRating { get; set; }
    }
}
