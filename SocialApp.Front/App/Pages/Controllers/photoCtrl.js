﻿angular.module('photos')
    .controller('photosCtrl', [
        '$scope',
        'photosService',
        function ($scope, photosService) {

            photosService.getPhotos()
                .then(function (response) {
                    $scope.photosData = response;
                }, function (error) {
                    console.log(error);
                });
        }]);