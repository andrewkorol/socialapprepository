﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SocialAppDomain.Models;
using SocialAppDomain.Models.PhotoModels;
using SocialAppBuisnessLogic;
using Microsoft.Practices.Unity;
using SocialAppDataAccess;
using SocialAppDomain.Interfaces;
using SocialAppDataAccess.Contracts;
using SocialAppDomain.Models.ReturnTypesModels;

namespace SocialApp.Controllers
{
    public class PhotosController : ApiController
    {
        private IUserPhotoRepository repository;
        private ICalculateRatingService service;
        public PhotosController(ICalculateRatingService service)
        {
            repository = new UserPhotoRepository(new ApplicationDbContext());
            this.service = service;
        }
        public void Post(LikedPhoto photo)
        {
            bool isLiked = repository.LikePhoto(photo);
            repository.SaveNewRating(service.CalculateRatingAndLikes(repository.GetPhotoRatingAndLikes(photo)), isLiked);
        }

        public UserPhotosInfo Get(string id)
        {
             return repository.GetPhotos(id);
        }
    }
}