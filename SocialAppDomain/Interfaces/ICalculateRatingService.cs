﻿using SocialAppDomain.Models.PhotoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialAppDomain.Interfaces
{
    public interface ICalculateRatingService
    {
        PhotoCalculatedRating CalculateRatingAndLikes(PhotoRatingAndLikes photo);
    }
}
