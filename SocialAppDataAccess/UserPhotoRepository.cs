﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SocialAppDomain.Models.PhotoModels;
using SocialAppDomain.Models;
using SocialAppDomain.Models.LikeModels;
using SocialAppDomain.Interfaces;
using SocialAppDataAccess.Contracts;
using SocialAppDomain.Models.ReturnTypesModels;

namespace SocialAppDataAccess
{
    public class UserPhotoRepository : IDisposable, IUserPhotoRepository
    {
        private ApplicationDbContext context;

        public UserPhotoRepository(ApplicationDbContext context)
        {
            this.context = context;
        }
        public UserPhotosInfo GetPhotos(string id)
        {
            return new UserPhotosInfo
            {
                userName = context.Users.SingleOrDefault(u => String.Equals(u.Id, id)).UserName,
                info = context.UserPhotos.Where(p => String.Equals(p.applicationUserId, id)).Select(p => new PhotoInfo
                {
                    whoLike = p.Likes.Select(l => l.ApplicationUser.UserName),
                    pathToPhoto = p.Name
                })
            };
        }
        public void InsertPhoto()
        {
            context.UserPhotos.Add(new UserPhoto
            {
                applicationUserId = "bdffdcda-1b02-44f4-86a4-1c82037772c7",
                Name = "/Photos/2.jpg"
            });
            context.SaveChanges();
        }
        public bool LikePhoto(LikedPhoto photo)
        {
            Like like = context.Likes.FirstOrDefault(l => String.Equals(l.applicationUserId, photo.whoLikeId) && l.userPhotoId == photo.photoId);
            if (like == null)
            {
                context.Likes.Add(new Like
                {
                    userPhotoId = photo.photoId,
                    applicationUserId = photo.whoLikeId
                });
                context.SaveChanges();
                return true;
            }
            else
            {
                context.Likes.Remove(like);
                context.SaveChanges();
                return false;
            }
        }

        public PhotoRatingAndLikes GetPhotoRatingAndLikes(LikedPhoto photo)
        {
            IEnumerable<UserPhoto> whoLikePhotos = context.UserPhotos.Where(p => String.Equals(p.applicationUserId, photo.whoLikeId));
            IEnumerable<UserPhoto> likedUserPhotos = context.UserPhotos.Where(p => String.Equals(p.applicationUserId, photo.likedUserId));
            return new PhotoRatingAndLikes
            {
                photoId = photo.photoId,
                currentRating = context.UserPhotos.FirstOrDefault(p => p.Id == photo.photoId).Rating,
                currentNumberOfLikes = context.UserPhotos.FirstOrDefault(p => p.Id == photo.photoId).Likes.Count(),
                numberOfLikesWho = whoLikePhotos.Select(p => p.Likes.Count()).Sum(),
                numberOfLikesWhom = likedUserPhotos.Select(p => p.Likes.Count()).Sum()
            };
        }


        public void SaveNewRating(PhotoCalculatedRating photo, bool isLiked)
        {
            UserPhoto userPhoto = context.UserPhotos.FirstOrDefault(p => p.Id == photo.Id);
            if (isLiked)
            {
                userPhoto.Rating += photo.newRating;
            }
            else
            {
                userPhoto.Rating -= photo.newRating;
            }
            context.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                {
                    context.Dispose();
                    context = null;
                }
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
