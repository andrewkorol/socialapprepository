﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SocialAppDomain.Models.PhotoModels;
using System.Collections;
using System.Collections.Generic;
using SocialAppDomain.Models.LikeModels;
using SocialAppDomain.Interfaces;

namespace SocialAppDomain.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser> , IApplicationDbContext
    {
        public ApplicationDbContext()
            : base("ApplicationDbContext")
        {
        }

        public DbSet<UserPhoto> UserPhotos { get; set; }
        public DbSet<Like> Likes { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}