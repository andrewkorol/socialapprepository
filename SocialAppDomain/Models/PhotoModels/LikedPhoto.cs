﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialAppDomain.Models.PhotoModels
{
    public class LikedPhoto
    {
        public int photoId { get; set; }
        public string likedUserId { get; set; }
        public string whoLikeId { get; set; }
    }
}
