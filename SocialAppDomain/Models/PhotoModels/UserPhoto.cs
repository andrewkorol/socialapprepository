﻿using SocialAppDomain.Models.LikeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialAppDomain.Models.PhotoModels
{
    public class UserPhoto
    {
        public UserPhoto()
        {
            Likes = new List<Like>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Rating { get; set; }
        public string applicationUserId { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual ICollection<Like> Likes { get; set; }

    }
}
