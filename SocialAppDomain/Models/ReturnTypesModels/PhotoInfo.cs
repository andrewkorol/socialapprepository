﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialAppDomain.Models.ReturnTypesModels
{
    public class PhotoInfo
    {
        public string pathToPhoto { get; set; }
        public IEnumerable<string> whoLike { get; set; } 
    }
}
