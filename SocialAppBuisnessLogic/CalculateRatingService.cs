﻿using SocialAppDomain.Interfaces;
using SocialAppDomain.Models.PhotoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialAppBuisnessLogic
{
   
    public class CalculateRatingService : ICalculateRatingService
    {
        public PhotoCalculatedRating CalculateRatingAndLikes(PhotoRatingAndLikes photo)
        {
            try
            {
                return new PhotoCalculatedRating
                {
                    Id = photo.photoId,
                    newRating = (photo.numberOfLikesWho * photo.numberOfLikesWhom) / photo.currentNumberOfLikes,
                };
            }
            catch (DivideByZeroException)
            {
                return new PhotoCalculatedRating
                {
                    Id = photo.photoId,
                    newRating = 1,
                };
            }
        }
    }
}
