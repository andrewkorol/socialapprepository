﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialAppDomain.Models.PhotoModels
{
    public class PhotoRatingAndLikes
    {
        public int photoId { get; set; }
        public int numberOfLikesWho { get; set; }
        public int numberOfLikesWhom { get; set; }
        public int currentNumberOfLikes { get; set; }
        public double currentRating { get; set; }
    }
}
