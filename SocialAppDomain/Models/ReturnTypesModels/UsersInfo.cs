﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SocialAppDomain.Models.ReturnTypesModels
{
    public class UsersInfo
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Hometown { get; set; }
    }
}