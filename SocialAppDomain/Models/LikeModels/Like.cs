﻿using SocialAppDomain.Models.PhotoModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocialAppDomain.Models.LikeModels
{
    public class Like
    {
        public int id { get; set; }
        public string applicationUserId { get; set; }
        public int userPhotoId { get; set; }
         
        public virtual ApplicationUser ApplicationUser { get; set; }
        public virtual UserPhoto UserPhoto { get; set; }
    }
}
