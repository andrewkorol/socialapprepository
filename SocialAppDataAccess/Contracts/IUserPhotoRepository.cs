﻿using SocialAppDomain.Models.PhotoModels;
using SocialAppDomain.Models.ReturnTypesModels;
using System.Collections.Generic;

namespace SocialAppDataAccess.Contracts
{
    public interface IUserPhotoRepository
    {
        bool LikePhoto(LikedPhoto photo);
        void SaveNewRating(PhotoCalculatedRating photo, bool isLiked);
        PhotoRatingAndLikes GetPhotoRatingAndLikes(LikedPhoto photo);
        UserPhotosInfo GetPhotos(string id);
        void InsertPhoto();
    }
}
